Cloudron Local App Development Environment
==========================================

🚀 Quickly startup [Cloudron](https://www.cloudron.io/) in a local VM for experiments without regrets.

<!-- TOC start (generated with https://github.com/derlin/bitdowntoc) -->

- [⚠️ Limitations](#limitations)
- [ℹ️ Prerequisites](#prerequisites)
- [Usage](#usage)
    * [1. ✔️ Preparation](#1--preparation)
    * [2. ▶️ Start and prepare VM](#2--start-and-prepare-vm)
    * [3. 🔑 register and login](#3--register-and-login)
    * [4. Control VM](#4-control-vm)
- [📝 Notes](#-notes)
- [🔗 Links](#-links)

<!-- TOC end -->

-----------------------------------------------------------------------------------------------------------------------
## Limitations
[Since Cloudron 7.6 ](https://forum.cloudron.io/topic/11052/cloudron-7-6-released?_=1708293689466&lang=de) MongoDB was updated to 5.0. 
Running Virtualbox on Windows and also Docker-Desktop installed, means having Hyper-V enabled.    
That means the required AVX CPU support by the host system cannot be used the guest system causing Cloudron installations to fail.  
This project provides the last version (7.5.2) running in this setup.

**From the Changelog 7.5.1 and 7.5.2**  
> **IMPORTANT**: This is the last release of Cloudron to support CPUs without AVX support. AVX support is required for MongoDB 5.0. See https://forum.cloudron.io/topic/8785/avx-support-in-your-vps-server for more information.



## ℹ️ Prerequisites
* [Vagrant](https://www.vagrantup.com)
* [Virtualbox](https://www.virtualbox.org/)

## Usage

### 1. ✔️ Preparation
Add entry `127.0.0.1    my.cloudron.local` to your 
- local DNS or 
- `/etc/hosts` file (*nix) or 
- `"C:\Windows\System32\drivers\etc\hosts"` (Windows)

on host system (not VM) for name resolution.

### 2. ▶️ Start and prepare VM
```bash
vagrant destroy -f && vagrant up && vagrant snapshot save initial
```

⌛ Setup takes some time! Be patient or watch the log:
```bash
vagrant ssh --no-tty --command 'tail -f /var/log/cloudron-setup.log'
```

### 3. 🔑 register and login
1️⃣ Navigate with your favourite browser to

* https://my.cloudron.local/ or
* https://console.cloudron.io/

and login or register with _cloudron.io_

2️⃣ Login to Cloudron local instance:
* 🌍 **URL:** https://my.cloudron.local/
* 👤 **User:** `cloudron `
* ⚿ **Password:** `cloudron `

3️⃣ Login via CLI:
```bash
️cloudron login my.cloudron.local --allow-selfsigned --username cloudron --password cloudron 
```

Login via SSH to host:
```bash
vagrant ssh -C sudo -s --
```

### 4. Control VM
⏪ Rollback to clean state `initial` after messing things up:
```bash
vagrant snapshot restore initial
```

⏸️ Suspend VM for later use:
```bash
vagrant halt
```

▶️ Start again:
```bash
vagrant up  
```

⏹ Destroy the VM and everything:
```bash
vagrant destroy -f
```
Do not forget to delete [subscription](https://console.cloudron.io/#/cloudrons)

## ❓ FAQ
* HTTP error 409 when provisioning VM  
💡 Delete existing [subscription](https://console.cloudron.io/#/cloudrons) for domain __cloudron.local__

## 📝 Notes
All contributions and suggestions appreciated.

## 🔗 Links
* https://docs.cloudron.io/installation/
* https://docs.cloudron.io/api.html
* https://git.cloudron.io/cloudron
* [Cloudron 7.5.2 package](https://releases.cloudron.io/box-7fe2de448e-7fe2de448e-7.5.2.tar.gz)
