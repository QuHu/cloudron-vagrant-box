#!/bin/bash
set -eu -o pipefail

if [[ -f ~/.initialized ]]; then
  echo "Already initialized." | tee -a /var/log/cloudron-setup.log
  exit 0
fi

apt install -qqy jo jq curl

cd /vagrant/resources || exit 1

echo "Domain-Config..." | tee --append /var/log/cloudron-setup.log
curl \
  --silent \
  --fail \
  --show-error \
  --insecure \
  --request POST \
  --header 'Content-Type: application/json' \
  --data-binary "@domainConfig.json" \
  --output /dev/null \
  https://my.cloudron.local/api/v1/provision/setup

echo "Activation..." | tee --append /var/log/cloudron-setup.log
curl \
  --silent \
  --fail \
  --show-error \
  --insecure \
  --request POST \
  --header 'Content-Type: application/json' \
  --data-binary "@activate.json" \
  --output /dev/null \
  https://my.cloudron.local/api/v1/provision/activate

echo  "Status..." | tee --append /var/log/cloudron-setup.log
curl \
  --silent \
  --fail \
  --show-error \
  --insecure \
  https://my.cloudron.local/api/v1/cloudron/status | jq -r .version

echo "Initialisation complete." | tee --append /var/log/cloudron-setup.log

touch ~/.initialized
